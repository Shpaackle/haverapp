import openpyxl
import time
from collections import OrderedDict
import simplejson as json


startTime = time.time()
wb = openpyxl.load_workbook('magic_items_full.xlsx')
#wb = openpyxl.load_workbook('example.xlsx')
endTime = time.time()

print("Took %s seconds to calculate." % (endTime - startTime))
sheet_names = wb.get_sheet_names()
current_sheet = sheet_names[0]
sheet = wb.active

#print("Number of rows: " + str(sheet.max_row) + "\nNumber of columns: " + str(sheet.max_column))
#print(sheet[1])
column_headers = sheet[1]
chl = len(column_headers)
row_count = sheet.max_row
column_count = sheet.max_column
if (chl != column_count) or column_count <= 0:
    print((chl != column_count))
    print(column_count <= 0)
    print("Length of column_headers is: " + str(chl) + str(type(chl)))
    print("Number of columns in sheet is: " + str(column_count) + str(type(column_count)))
    exit(1)
else:
    column_count = chl

print(f"Number of columns is: {column_count}")
print(f"Number of rows is: {row_count}")


#temp_variable = column_headers[0].value
#print(f"column header 0 is: {temp_variable} and is of type {type(temp_variable)}" )

startTime = time.time()
items_list = []
items_dict = {}
part_per = row_count // 20
remaining_amount = row_count % 20

for row in range(2, row_count):

    item = OrderedDict()
    row_values = sheet[row]
    item['My ID'] = row - 1
    for i in range(0, column_count):
        header = column_headers[i]
        item[header.value] = row_values[i].value

    items_list.append(item)
endTime = time.time()
print("Took %s seconds to calculate." % (endTime - startTime))

startTime = time.time()
end_count = 0
for i in range(0,20):
    start_count =i * part_per
    end_count = start_count + part_per - 1
    to_dump = items_list[start_count:end_count]
    j = json.dumps(to_dump)

    filename = "magic_items_" + str(i) + ".json"
    with open(filename, 'w') as f:
        f.write(j)
'''
start_count = end_count
end_count += remaining_amount
to_dump = items_list[start_count:end_count]
j = json.dumps(to_dump)
with open("magic_items_21.json", 'w') as f:
    f.write(j)
'''
endTime = time.time()
print("Took %s seconds to calculate." % (endTime - startTime))
