"""This models our data for Mongo."""
# TODO add class to store custom ID's
from mongoengine import *


class User(Document):
    """Someone that uses the application."""
    
    email = EmailField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    user_id = IntField()

    def __str__(self):
        msg = "" + self.first_name + " " + self.last_name
        return msg


class CatalogItem(Document):
    """A generic item."""
    
    name = StringField(max_length=120, required=True)
    category = StringField(max_length=120)
    family = StringField(max_length=120)
    subcategory = StringField(max_length=120)
    aura = StringField(max_length=120)
    caster_level = StringField(max_length=20)
    slot = StringField(max_length=120)
    price = StringField(max_length=120)
    weight = StringField(max_length=120)
    description = StringField()
    requirements = StringField()
    cost = StringField(max_length=120)
    group = StringField(max_length=120)
    source = StringField(max_length=120)
    alignment = StringField(max_length=120)
    intelligence = StringField(max_length=120)
    wisdom = StringField(max_length=120)
    charisma = StringField(max_length=120)
    ego = StringField(max_length=120)
    communication = StringField(max_length=120)
    senses = StringField(max_length=120)
    powers = StringField()
    magic_items = StringField()
    full_text = StringField()
    destruction = StringField()
    minor_artifact_flag = BooleanField()
    major_artifact_flag = BooleanField()
    abjuration = BooleanField()
    conjuration = BooleanField()
    divination = BooleanField()
    enchantment = BooleanField()
    evocation = BooleanField()
    necromancy = BooleanField()
    transmutation = BooleanField()
    aura_strength = StringField(max_length=120)
    weight_value = FloatField()
    price_value = IntField()
    cost_value = IntField()
    languages = StringField(max_length=120)
    base_item = StringField(max_length=120)
    link_text = StringField(max_length=120)
    id = IntField()
    mythic = BooleanField()
    legendary_weapon = BooleanField()
    illusion = BooleanField()
    universal = BooleanField()
    scaling = StringField(max_length=120)
    
    meta = {'allow_inheritance': True}

    def __str__(self):
        return self.name


class InventoryItem(Document):
    """A specific item."""
    
    name = StringField(max_length=120, required=True)
    acquired_date = DateTimeField()
    acquired_from = StringField(max_length=120)
    created_by = ReferenceField(User)
    notes = StringField()
    base = ReferenceField(CatalogItem)
    special = ListField(StringField(max_length=20))
    plus = ListField(StringField(max_length=2))
    material = StringField(max_length=20)

    meta = {'allow_inheritance': True}

    def __str__(self):
        return self.name


class Bonus(EmbeddedDocument):
    """Used to convey stat boosts, skill boosts, etc."""
    
    name = StringField(max_length=50)
    value = IntField()

    """
    def __str__(self):
        if self.value > 0:
            msg = "-" + str(self.value) + " " + self.name
            return msg
        elif self.value > 0:
            msg = "+" + str(self.value) + " " + self.name
            return msg
    """


class Gear(CatalogItem):
    """Anything that can have a bonus."""
    
    bonus = ListField(EmbeddedDocumentField(Bonus))
    

class Weapon(CatalogItem):
    """A generic weapon of its type."""
    
    damage_s = StringField(max_length=20)
    damage_m = StringField(max_length=20)
    critical = StringField(max_length=20)
    range_increment = StringField(max_length=20)
    damage_type = StringField(max_length=50)
    size = StringField(max_length=20)
    encumberance = StringField(max_length=20)
    usefulness = StringField(max_length=20)
    training = StringField(max_length=20)
    

class Armor(CatalogItem):
    """A generic armor of its type."""
    
    armor_shield_bonus = StringField(max_length=20)
    armor_type = StringField(max_length=20)
    maximum_dex_bonus = StringField(max_length=20)
    armor_check_penalty = StringField(max_length=20)
    arcane_spell_failure_chance = StringField(max_length=20)
    speed_30 = StringField(max_length=20)
    speed_20 = StringField(max_length=20)
