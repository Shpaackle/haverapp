import os
import sys


def menu(msg, options, name=None):
    #os.system('clear')
    print('\n\n\n')
    if len(msg) > 1:
        for msg_text in msg:
            print(msg_text)
    else:
        print(msg[0], end='')
    print('\n')
    print("Please choose the menu you want to start:")
    num_index = 1
    for option_text in options:
        print(str(num_index) + '. ' + option_text)
        num_index += 1

    if name:
        if name == "party":
            print("9. Character")
        elif name != "main":
            print("9. Back")
        print("\nQ to quit")
    else:
        print("0. Quit")


def main_menu():
    message = ["Welcome to the Inventory Manager"]
    options = ["User", "Character/Party", "Loot"]
    name = "main"
    menu(message, options, name)


def user_menu():
    message = ["User Screen"]
    options = ["New User", "Load User", "Guest", "Character/Party", "Loot"]
    name = "user"
    menu(message, options, name)


def char_menu():
    message = ["Character Screen"]
    options = ["New Character", "Load Character", "Party", "Loot", "User"]
    name = "character"
    menu(message, options, name)

def party_menu():
    message = ["Party Screen"]
    options = ["New Party", "Load Party", "Edit Party", "User", "Loot"]
    name = "party"
    menu(message, options, name)

def loot_menu():
    message = ["Loot Screen"]
    options = ["New Loot", "Load Loot", "Edit Loot", "Character/Party", "Loot"]
    name = "loot"
    menu(message, options, name)

def edit_loot_menu():
    message = ["Edit Loot Screen"]
    options = []
    name = "edit_loot"
    menu(message, options, name)

def edit_party_menu():
    message = ["Edit Party Screen"]
    options = []
    name = "edit_party"
    menu(message, options, name)

def edit_user_menu():
    message = ["Edit User Screen"]
    options = ["Change Name", "Change Email", "Change Password", "Delete User"]
    name = "edit_user"
    menu(message, options, name)

"""
# Execute menu
# TODO add previous menu variable
def exec_menu(choice, previous=None):
    os.system('clear')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    elif ch == '9':
      # TODO add way to go back one screen
        menu_actions[ch]()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print("Invalid selection, please try again.\n")
            menu_actions['main_menu']()
    return
"""
