import json


class JSONManager:
    def __init__(self):
        self.json_list = {}

    def add_json_data(self, file_name):
        d = json.load(file_name)
        if not d:
            print(f"{file_name} could not be opened!")
            exit(1)
        else:
            print(f"Loading {file_name} was successful.")

