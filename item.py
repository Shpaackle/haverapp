from enum import Enum


"""
{
    "category": "Item",
    "family": "Trade Goods",
    "reference": "SRD 3.5 Equipment",
    "full_text": null,
    "cost": "1 sp",
    "name": "One pound of iron"
}
"""


class Item:
    def __init__(self, name):
        self.name = name
