'''
1. Add item to inventory
2. Search by:
    A. name
    B. category
    C. Reference
    D. cost
    E. Family
3. Create new party
4. Remove item from inventory
5. Buy item from shop
6. Sell item to shop
7. Create new container
8. Move item between containers
9. Create magic item
10. Create loot chart
11. Create loot list
12. Assign loot to party/character
13. Save inventory
14. Load inventory


"""
Class User:
    string user_name
    Int user_id
    Characters = [ ]


Class Character:
    string char_name
    int char_id
    containers = inventory component
    stats = { }


Class Container
    int max_capacity
    int current_capacity
    contains = { }


Class Item:
    int item_id
    string item_name
    string Description
    float cost
    float weight
    string type
    components = [ ]

def add_component
def make_container
def load_json
def save_json


Class Armor(component):
    string subtype
    material
    subclass
    magical
    masterwork
    armor_bonus
    size
    armor_check_penalty
    spell_failure_chance
    max_dex_bonus
    special_qualities
    special abilities


Class Weapon(component):
    string subtype
    material
    subclass
    magical
    masterwork
    damage
    damage_type
    size
    critical
    range
    special qualities
    special abilities


Class Wondrous(component):
    body slot
    category
    aura {'strength' : '', 'school': ''}
    caster level
    special qualities
    special abilities


Class Wand(component):
    spell
    caster level
    charges used
    crafted/purchased/found


Class Inventory(component):
    owned by
    containers = { }
"""

"""
def create item():
    assign new ID
    copy from JSON data
    add necessary components
    add to inventory database

def move_item(from, to, qty):
    check if qty of it is in from
    if qty would be 0
        pop item
    else
        copy item
        subtract qty
    check capacity of container before adding
    if item in to
        add qty
    else
        add item to inventory list
        
def create_user
    initialize user class
    user.name = input('Enter user name: ')
    user.password = input('Please choose password: ')
    user.email = input('Please enter email: ')
    return user
    
def create_character()
    new_char = initialize character class
    new_char.name = input("Enter character name: ")
    new_char.system = input("Enter game system: ")
    
def create_party()
    new_party = intialize party class
    finished = false
    while !finished:
        choice = input("Enter Q to quit")
        if choice != 'Q' or 'q'
            create new charaacter
            add to party list
        else
            finished = true
"""

"""
print("Welcome to the Inventory Manager")
print("Would you like to create a new account or log into a current one")
print("1. New   2. Login   ")
choice = input('> ')
while choice != 1:
    print("Invalid choice, try again.")
    choice = input('> ')
clear screen 
"""

"""
Class MagicItemSlot(Enum):
    Head,
    Headband,
    Eyes,
    Shoulders,
    Neck,
    Chest,
    Body,
    Armor,
    Belt,
    Wrists,
    Hands,
    Ring1,
    Ring2

    
Class Category(Enum):
    minor,
    medium,
    major
"""
'''