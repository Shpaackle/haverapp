import logging as LOG


class Party():
    def __init__(self):
        self.party_name = "New"
        self.characters = []
        self.user = ''
        self.party_id = -1

        LOG.debug("Created new party. (" + self.party_name + ')')


    def create_new_party(self, user):
        print("Enter party name: ")
        party_name = input(" >> ")
        #TODO check for valid name

        # TODO assign new user_id number
        self.party_id = 1

        return self

    def add_character(self, character):
        LOG.debug("Entering add character...")
        self.characters.append(character)
        added_character = self.characters[len(self.characters) - 1]
        if added_character:
            print("Added character" + added_character.name)
        else:
            LOG.error("Added character doesn't exist.")
            exit("Add character failed")
        LOG.debug("Exiting add character")

    def load_party(self):
        LOG.debug("Entering load user...")
        print("Loaded user")
        LOG.debug("Exiting load user")

    def load_guest(self):
        LOG.debug("Entering load guest...")
        LOG.debug("Exiting load guest")
