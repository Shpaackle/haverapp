try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

config = {
	'description': 'Inventory Manager',
	'author': 'Shpaackle',
	'url': 'URL',
	'download_url': 'download_url',
	'author_email': 'Shpaackle@gmail.com',
	'version': '0.1',
	'install_requires': ['nose'],
	'packages': ['NAME'],
	'scripts': [],
	'name': 'haverapp'
}

setup(**config, install_requires=['mongoengine'])