from mongoengine import *
import sys
import os.path

sys.path.append(os.path.join(os.path.abspath(os.path.curdir), 'src'))


def connect_database():
    alias = connect(host="mongodb://dan:shoehornbravosumocube@cluster0-shard-00-00-2axto.mongodb.net:27017,"
                    "cluster0-shard-00-01-2axto.mongodb.net:27017,"
                    "cluster0-shard-00-02-2axto.mongodb.net:27017/haverapp?ssl=true&replicaSet=Cluster0-shard-0"
                    "&authSource=admin")
    if alias:
        print("successfully connected to database")
    return alias
