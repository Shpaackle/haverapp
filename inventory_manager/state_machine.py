from enum import Enum


class States(Enum):
    MAIN_MENU = "main"
    USER_MENU = "user"
    CHAR_MENU = "char"
    LOOT_MENU = "loot"
    PARTY_MENU = "party"
    PRINT_USER = "print"
