import logging as LOG


def get_new_id(thing, id_db, id_type=None, duplicate_allowed=False):
    # TODO add ability to check if item is already in db
    if id_type:
        new_id = len(id_db[id_type])
        if new_id >= 0:
            LOG.debug("new ID created successfully")
            id_db[id_type].append(thing)
        return new_id
    LOG.error("ID not assigned. (get_new_id)")
    return -1
