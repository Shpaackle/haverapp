from enum import Enum

# enum for type of items
class ItemTypes(Enum):
    ARMOR = 1,
    WEAPON = 2,
    POTION = 3,
    MISC = 4
    
class Sizes(Enum):
    FINE = -4,
    DIMUNITIVE = -3,
    TINY = -2,
    SMALL = -1,
    MEDIUM = 0,
    LARGE = 1,
    HUGE = 2,
    GARGANTUAN = 3,
    COLOSSAL = 4
    
    
class Location:
    def __init__(self, name, description, owned_by, capacity):
        self.name = name
        self.description = description
        self.owned_by = owned_by
        self.max_capacity = capacity
        self.current_capacity = 0

    def __str__(self):
        return 'Location'


# definition for Item class
class Item:
    def __init__(self, name, item_type, description=None, item_image=None, categories=None, notes=None, cost=None, source=None, weight=None, stats=None, size=Sizes.MEDIUM):
        self.name = name
        self.item_type = item_type
        self.description = item_type
        self.item_image = item_image
        self.notes = notes
        self.cost = cost
        self.source = source
        self.weight = weight
        self.stats = stats
        self.location = None
        self.categories = []
        self.size = size
        
        # nessy.info/?p=82
        # JSON file of items includes:
        # category, cost, family, full_text, name, reference
        # category, cost, critical, dmg_m, dmg_s, family, full_text, name, range_increment, reference, subcategory, type, weight
        
        
        
    def change_location(self, new_location):
        self.location = new_location
        
# definition for 
class Inventory:
    def __init__(self, capacity, owned_by):
        self.capacity = capacity
        self.owned_by = owned_by
        self.locations = []
        self.items = []
        
    def add_item(self, item):
        if len(self.items) >= self.capacity:
            print('Inventory full, item not added.')
        else:
            print('Added {0} to inventory'.format(item.name))
            self.items.append(item)
            
    def remove_item(self, item_name):
        removed_index = self.find_item_by_name(item_name)
        if not self.items:
            print('Inventory is empty, cannot remove item.')
        else:
            print('Removed {0} from inventory'.format(item_name))
            del self.items[removed_index]
            #self.items.remove(removed_index)
    
    def move_item(self, item, new_location):
        old_location = item.location
        item.change_location(new_location)
        print('{0} moved from {1} to {2} successfully.'.format(str.capitalize(item.name), old_location, new_location))

    def find_item_by_name(self, item_name):
        item_index = 0
        for item in self.items:
            if item.name == item_name:
                return item_index
            item_index =+ 1
        
        return None
        
    def add_location(self, name, capacity, description=None, owned_by=None):
        self.locations.append(Location(name, description, owned_by, capacity))
        print('Added location: ' + self.locations[-1])
    
    def display_inventory_in_location(self, location):
        print('\n')
        print(self.owned_by + ': ' + self.locations)
        if len(self.items) == 0:
            print('{0} inventory is empty.'.format(self.owned_by))
        else:
            for item in self.items:
                print(item.name)

        
capacity = 4

owned = 'Party'

main_inventory = Inventory(capacity, owned)

main_inventory.add_location('Backpack',30)
location = main_inventory.locations[0]

main_inventory.add_item(Item('Healing Potion', ItemTypes.POTION))
main_inventory.add_item(Item('Long sword', ItemTypes.WEAPON))
main_inventory.add_item(Item('rations', ItemTypes.MISC))
main_inventory.add_item(Item('chain mail', ItemTypes.ARMOR))

main_inventory.display_inventory_in_location(main_inventory.locations[0])

main_inventory.remove_item('Healing Potion')

main_inventory.display_inventory_in_location('Backpack')

print('finished')




