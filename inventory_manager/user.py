import logging as log

# from database_functions import assign_id


class User:
    def __init__(self):
        self.first_name = "New"
        self.last_name = "Guest"
        self.password = None
        self.email = None
        self.id_num = -1
        self.characters = []
        self.parties = []
        self.is_guest = True
        log.debug("Created new user.")

    def __str__(self):
        output = self.first_name + ' ' + self.last_name
        output += '\n'
        output += "User ID: " + str(self.id_num)
        return output

    def create_new_user(self, user_db):
        print("Enter first name: ")
        first_name = input(" >> ")
        print("Enter last name: ")
        last_name = input(" >> ")
        # TODO check for valid name

        print("Enter password: ")
        password = input(" >> ")
        # TODO check for valid password

        print("Enter email")
        email = input(" >> ")
        # TODO use regex to test for valid email
        # TODO check if email already exists in database
        # if user_db(email__contains=email)
            # print error message
            # go to load function
        self.first_name = first_name
        self.last_name = last_name
        self.password = password
        self.email = email

        # TODO assign new user_id number
        # self.id_num = assign_id.get_new_id(self, id_db, id_type="user")
        # self.id_num = 1

        self.is_guest = False

        return self

    def change_password(self, old, new):
        if old != self.password:
            print("Wrong password entered, changes not saved")
        else:
            self.password = new
            print("Password successfully changed")

    def change_email(self, new):
        # TODO send email to old address
        self.email = new
        print("Email changed, please reply to confirmation email.")

    def add_character(self, character):
        log.debug("Entering add character...")
        self.characters.append(character)
        print("Added character")
        log.debug("Exiting add character")

    def add_party(self, party):
        log.debug("Entering add party...")
        self.parties.append(party)
        print("Added party " + str(party))
        log.debug("Exiting add party")

    def load_user(self, db):
        log.debug("Entering load user...")
        loaded = None
        email = input("Please enter email of user to load")
        log.debug("User entered %s as email." % (email))
        # TODO check for valid input
        for user in db:
            if user.email == email:
                log.debug("Found user succesfully")
                loaded = user
                break

        if loaded:
            self.first_name = loaded.first_name
            self.last_name = loaded.last_name
            self.email = loaded.email
            self.id_num = loaded.id
            log.debug("copied information from database, returning to main loop")
            return self
        else:
            log.error("Exiting load user without loading")
            print("Failed to load user. Would you like to quit?")
            answer = input("(Y/N) > ")
            if answer.lower == "y":
                exit(-1)

    def load_guest(self):
        log.debug("Entering load guest...")
        log.debug("Exiting load guest")
