import pprint
from random import randint


class Stats():
    def __init__(self, name):
        self.name = name
        # TODO modifiers as class or component
        self.modifiers = {'racial': 0, 'enhancement': 0, 'unnamed': 0, 'damage': 0, 'drain': 0}
        self.current_value = None
        self.max_value = None
        self.base_value = None

    def assign_base_value(self, name, stat):
        print(str(name) + ": " + str(stat))
        if self.name == name:
            self.base_value = stat


class User():
    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password
        self.characters = {}
        self.user_id = self.assign_id()

    @staticmethod
    # TODO move from class to global
    def assign_id():
        # TODO assign new user id properly
        user_id = randint(1, 1000)
        return user_id

    def change_email(self, new, old):
        if old == new:
            return
        self.email = new

    def change_password(self, new, old):
        if old == new:
            return
        self.password = new

    def print_character(self, name, pp):
        char_print = self.characters[name]
        pp.pprint("Name: " + char_print.name)
        pp.pprint("Owner: " + char_print.owner)

        for stat_name in char_print.stat_list:
            stat = char_print.stats[stat_name]
            pp.pprint(stat.name + " " + stat.base_value)

        pp.pprint(char_print.inventory)


class Character():
    stat_list = ["Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"]

    def __init__(self, name, owner):
        self.name = name
        self.owner = owner
        self.stats = {}
        for stat in self.stat_list:
            self.stats[str(stat)] = -1

        self.inventory = {}

    def change_owner(self, new, old):
        if new == old:
            return

        # TODO check if user exits
        self.owner = new

    def change_stat(self, stat_name, stat_value, value_type):
        pass

    def assign_stats(self, stats):
        for stat in self.stat_list:
            base_stat = Stats(str(stat))
            base_stat.assign_base_value(str(stat), stats[str(stat)])
            self.stats[str(stat)] = base_stat


def create_new_user():
    name = input("Enter user name: ")
    # TODO check if name is valid

    email = input("Enter email: ")
    # TODO check if email is valid

    password = input("Enter password")
    # TODO check if password is valid

    return User(name, email, password)


def create_new_character(user):
    name = input("Please enter character name: ")
    # name = "Johan"
    owner = user.name
    stat_list = ["Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"]
    stats = {}
    for stat in stat_list:
        # new_stat = randint(10,18)
        new_stat = input("Enter value for " + stat + ": ")
        stats[stat] = new_stat

    # character = {'name': name, 'owner': owner, 'stats': stats}

    user.characters[name] = Character(name, owner)
    user.characters[name].assign_stats(stats)


def main():
    user = create_new_user()
    create_new_character(user)
    pp = pprint.PrettyPrinter()
    pp.pprint(user.characters.keys())
    name = input("Enter name of character to print: ")
    user.print_character(name, pp)


if __name__ == "__main__":
    main()