from state_machine import States
import logging as LOG


def handle_input(user_input, current_state=None, prev_state=None):
    if user_input:
        if current_state == States.USER_MENU:
            return handle_user_menu(user_input, prev_state)
        elif current_state == States.LOOT_MENU:
            return handle_loot_menu(user_input, prev_state)
        elif current_state == States.CHAR_MENU:
            return handle_char_menu(user_input, prev_state)
        elif current_state == States.PARTY_MENU:
            return handle_party_menu(user_input, prev_state)

    LOG.debug("About to return an empty action value.")
    return {}

def handle_main_menu(user_input):
    if user_input:
        key_char = user_input[0].lower()
        if key_char == "1":
            return {"user": True}
        elif key_char == "2":
            return {"char": True}
        elif key_char == "3":
            return {"loot": True}
        elif key_char == "0" or "e" or 'q':
            return {"exit": True}

def handle_user_menu(user_input, prev_state):
    LOG.debug("Handle user menu")
    options = ["New User", "Load User", "Guest", "Character/Party", "Loot"]
    if user_input:
        if user_input == "check":
            print("Checking user database")
            return {"check": "user"}
        elif user_input == "edit":
            print("editing user")
            return {"edit": "user"}
        LOG.debug("user_input found")
        key_char = user_input[0].lower()
        LOG.debug("key_char = (%s%%)" % (key_char))
        if key_char == "1":
            LOG.debug("New User")
            print("New User")
            return {"new": True}
        elif key_char == '2':
            print("Load User")
            return {"load": True}
        elif key_char == '3':
            print("Guest")
            return {"guest": True}
        elif key_char == '4':
            print("Character/Party")
            return {"change": States.CHAR_MENU}
        elif key_char == '5':
            print("Loot")
            return {"change": States.LOOT_MENU}
        elif key_char == '6':
            print("Print user")
            return {"print": "user"}
        elif key_char == '9':
            print("Go Back")
            return {"change": prev_state}
        elif key_char == '0' or 'q':
            print("User menu - Quit")
            return {"quit": True}

    return {}

def handle_char_menu(user_input, prev_state):
    options = ["New Character", "Load Character", "Party", "Loot", "User"]
    if user_input:
        key_char = user_input[0].lower()
        if key_char == "1":
            print("New Character")
            return {"new": True}
        elif key_char == 2:
            print("Load Character")
            return {"load": True}
        elif key_char == 3:
            print("Party")
            return {"change": States.PARTY_MENU}
        elif key_char == 4:
            print("Loot")
            return {"change": States.LOOT_MENU}
        elif key_char == 5:
            print("User")
            return {"change": States.USER_MENU}
        elif key_char == 9:
            print("Go Back")
            return {"change": prev_state}
        elif key_char == 0 or 'q':
            print("Quit")
            return {"quit": True}

    return {}

def handle_loot_menu(user_input, prev_state):
    return

def handle_party_menu(user_input, prev_state):
    return