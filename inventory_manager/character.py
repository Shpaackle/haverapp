import logging as LOG

from database_functions.assign_id import get_new_id


class Character():
    def __init__(self, first="New", last="Guest", user="Guest", party=None, id_num=-1):
        self.first_name = first
        self.last_name = last
        self.full_name = self.first_name + ' ' + self.last_name
        self.user = user
        self.party = party
        self.id_num = id_num

        LOG.debug("Created new character.")

    def __str__(self):
        output = self.full_name
        output += '\n'
        output += "ID: " + str(self.id_num)
        print(output)

    def create_new_character(self, user, id_db):
        name_valid = False
        first_name = ''
        last_name = ''
        loop_count = 0

        LOG.debug("Entering loop to ask for character name")
        while name_valid != True:
            if first_name == '':
                print("Enter character's first name: ")
                first_name = input(" >> ")
            if last_name == '':
                print("Enter character's last name: ")
                last_name = input(" >> ")

            if first_name and last_name:
                LOG.debug("Character first and last name entered successfully.")
                LOG.debug("Trying to exit loop...")
                name_valid = True
            #TODO check for valid name

            # to avoid infinite loop
            loop_count += 1
            if loop_count >= 15:
                LOG.error("Aborted because of too many iterations trying to get character name.")
                LOG.error("Loop count reached " + str(loop_count))
                exit("Too many iterations in create new character " + str(loop_count))

        LOG.debug("Exited loop successfully.")

        self.id_num = get_new_id(id_db, "char")
        LOG.debug("Assigned character id")

        LOG.debug("Returning new character: " + str(self))
        return self

    def change_name(self):
        LOG.debug("Entering change name...")
        #TODO implement change name function
        print("Name changed.")
        LOG.debug("Exiting change name.")

    def add_character(self, character):
        LOG.debug("Entering add character...")
        self.characters.append(character)
        print("Added character")
        LOG.debug("Exiting add character")

    def change_party(self, new=None, old=None):
        LOG.debug("Entering change party...")

        # error checking to make sure new & old party exist and are not the same value
        if new == None:
            LOG.error("New party does not exist. ")
        elif old == None:
            LOG.error("Old party does not exist. ")
        elif new == old:
            print("Character already in party.")
            LOG.error("Tried to add character to party failed. New party and old party are same value.")
        else:
            self.party = new
            LOG.debug("Changed party from " + old + " to " + new + '.')

        LOG.debug("Exiting change party")

    def load_char(self):
        LOG.debug("Entering load character...")
        #TODO implement load character
        print("Loaded character")
        LOG.debug("Exiting load character")
