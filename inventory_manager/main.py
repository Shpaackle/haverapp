#! /usr/bin/env python3
# title           :menu.py
# description     :This program displays an interactive menu on CLI
# author          :
# date            :
# version         :0.1
# usage           :python menu.py
# notes           :
# python_version  :2.7.6
# =======================================================================

# Import the modules needed to run the script.
import logging as LOG

LOG.basicConfig(level=LOG.DEBUG, format=' %(asctime)s - %(levelname)s - %(message)s')
LOG.debug("Start of program")
import os

import menus
from input_handlers import handle_main_menu, handle_input
from user import *
from character import *
from state_machine import States
from item_functions import item_database
from database_functions import db_interaction as my_db
from database_functions import model

def run_app(current_state, prev_state):
    if current_state == States.USER_MENU:
        menus.user_menu()
        user_input = input("> ")
        handle_input(user_input, current_state, prev_state)
    elif current_state == States.CHAR_MENU:
        menus.char_menu()
        user_input = input("> ")
        handle_input(user_input, current_state, prev_state)
        print("Char Menu")
    elif current_state == States.LOOT_MENU:
        menus.loot_menu()
        user_input = input("> ")
        handle_input(user_input, current_state, prev_state)
        print("Loot Menu")
    elif current_state == States.PARTY_MENU:
        menus.party_menu()
        user_input = input("> ")
        handle_input(user_input, current_state, prev_state)
        print("Party Menu")


def main():
    # TODO move all main variables in separate function
    user = User()
    working_char = Character()
    party = {}
    working_inventory = {}

    current_state = States.MAIN_MENU
    prev_state = None

    is_running = True
    show_main_menu = True

    # TODO combine this database with mongo_db
    id_database = {
        "user": [],
        "char": [],
        "party": [],
        "loot": [],
        "item" : []
    }

    mongo_db = item_database.connect_database()
    collection = mongo_db.test_collection

    # Check for valid mongo database connection
    if mongo_db:
        LOG.debug("Connected to mongo database successfully")
        print(mongo_db)
        print(collection)
        answer = input()
        if answer == '6':
            weapons = model.CatalogItem.objects(family__contains="Weapon")
            for item in weapons:
                print(item.name)
                dir(item)
    else:
        LOG.error("Failed to connect to database")
        q = input("Quit? Y/N")
        if q.lower() == 'y':
            exit(-1)

    # begin main loop
    while is_running:
        os.system('clear')

        # handle main menu
        if current_state == States.MAIN_MENU:
            # Display main menu
            menus.main_menu()
            show_main_menu = False

            user_input = input(" > ")
            action = handle_main_menu(user_input)

            # Check value in action
            user_menu = action.get("user", False)
            char_menu = action.get("char", False)
            loot_menu = action.get("loot", False)
            quit_app = action.get("exit", False)

            # Assign previous state
            prev_state = current_state

            if quit_app:
                LOG.debug("Current state is " + str(current_state))
                LOG.debug("Previous state is " + str(prev_state))
                LOG.debug("Exiting application")
                is_running = False
                continue
            elif user_menu:
                current_state = States.USER_MENU
                LOG.debug("Changing current state to USER_MENU")
            elif char_menu:
                current_state = States.CHAR_MENU
                LOG.debug("Changing current state to CHAR_MENU")
            elif loot_menu:
                LOG.debug("Changing current state to LOOT_MENU")
                current_state = States.LOOT_MENU

            LOG.debug("Current state is " + str(current_state))
            LOG.debug("Previous state is " + str(prev_state))

        action = {}
        # TODO move handle_input out of if statements?
        if current_state == States.USER_MENU:
            LOG.debug("Entering user menu")
            menus.user_menu()
            user_input = input("> ")
            action = handle_input(user_input, current_state, prev_state)
        elif current_state == States.CHAR_MENU:
            menus.char_menu()
            user_input = input("> ")
            action = handle_input(user_input, current_state, prev_state)
            print("Char Menu")
            break
        elif current_state == States.LOOT_MENU:
            menus.loot_menu()
            user_input = input("> ")
            action = handle_input(user_input, current_state, prev_state)
            print("Loot Menu")
            break
        elif current_state == States.PARTY_MENU:
            menus.party_menu()
            user_input = input("> ")
            action = handle_input(user_input, current_state, prev_state)
            print("Party Menu")
            break

        log_message = "Handled input. Action is (" + str(action) + ')'
        if action:
            LOG.debug(log_message)
        else:
            LOG.ERROR(log_message)

        # Check if QUIT option was selected
        quit_app = action.get("quit")
        if quit_app:
            LOG.debug("Exiting application.  Goodbye!")
            is_running = False
            # TODO add quitting application function
            continue

        # Check if GUEST option was selected
        guest = action.get("guest")
        if guest:
            user = user.load_guest()

        # Check if CHANGE state was selected

        change_state = action.get("change")
        if change_state:
            prev_state = current_state
            current_state = change_state

        # Check if NEW option was selected
        new_action = action.get("new", False)
        if new_action:
            if current_state == States.USER_MENU:
                # create new user
                LOG.debug("New action: Create new user")
                user = user.create_new_user(model.User.objects)

                # save to mongo database
                my_db.save_to(user, "user", mongo_db)

                user.id_num = user.id

            elif current_state == States.CHAR_MENU:
                LOG.debug("New action: Create new character")
                working_char = working_char.create_new_character(user, id_database)
            elif current_state == States.PARTY_MENU:
                LOG.debug("New action: Create new party")
                party["new"] = "New"
            elif current_state == States.LOOT_MENU:
                LOG.debug("New action: Create new loot")
                working_inventory["new"] = "New"

        # Check if LOAD option was selected
        load_action = action.get("load", False)
        if load_action:
            if current_state == States.USER_MENU:
                LOG.debug("Load action: Load new user")
                user = user.load_user(model.User.objects)
            elif current_state == States.CHAR_MENU:
                LOG.debug("Load action: Load new character")
                working_char.load_char()
            elif current_state == States.PARTY_MENU:
                LOG.debug("Load action: Load new party")
                party["load"] = "Load"
            elif current_state == States.LOOT_MENU:
                LOG.debug("Load action: Load new loot")
                working_inventory["load"] = "Load"

        # Check if PRINT option was selected
        print_what = action.get("print", False)
        if print_what == "user":
            print(str(user))

        # Check if CHECK option was selected
        check_what = action.get("check", False)
        if check_what == "user":
            my_db.check_database(user, "user", model.User.objects)

# =======================
#      MAIN PROGRAM
# =======================

# Main Program
if __name__ == "__main__":
    main()


"""
#�Main definition - constants
#menu_actions  = {}

# =======================
#     MENUS FUNCTIONS
# =======================

# Main menu
def main_menu():
    os.system('clear')
    
    print("Welcome to the Inventory Manager,\n")
    print("Please choose the menu you want to start:")
    print("1. User")
    print("2. Character/Party")
    print("3. Loot creation")
    print("\n0. Quit")
    choice = input(" >>  ")
    exec_menu(choice)

    return

# Execute menu
# TODO add previous menu variable
def exec_menu(choice):
    os.system('clear')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    elif ch == '9':
      # TODO add way to go back one screen
        menu_actions[ch]()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print("Invalid selection, please try again.\n")
            menu_actions['main_menu']()
    return

# User Menu 
def user_menu():
  print("User Menu \n")
  print("1. New User")
  print("2. Load User")
  print("3. Delete User")
  print("9. Back")
  print("0. Quit")
  choice = input(" >>  ")
  if choice == '9' or '0':
    exec_menu(choice)
  else:
    exec_menu(choice + "_u")
  return


# Character/Party Menu 
def char_menu():
  print("Character/Party Menu \n")
  print("1. New Party")
  print("2. Load Party")
  print("3. New Character")
  print("4. Load Character")
  print("9. Back")
  print("0. Quit")
  choice = input(" >>  ")
  if choice == '9' or '0':
    exec_menu(choice)
  else:
    exec_menu(choice + "_c")
  return
  
# Loot menu
def loot_menu():
  print("Loot Menu \n")
  print("1. New User")
  print("2. Load User")
  print("3. Delete User")
  print("9. Back")
  print("0. Quit")
  choice = input(" >>  ")
  if choice == '9' or '0':
    exec_menu(choice)
  else:
    exec_menu(choice + "_l")
  return

# Back to main menu
def back():
    menu_actions['main_menu']()

# Exit program
def exit():
    sys.exit()

# =======================
#    MENUS DEFINITIONS
# =======================

# Menu definition
menu_actions = {
    'main_menu': main_menu,
    '1': user_menu,
    '2': char_menu,
    '3': loot_menu,
    '9': back,
    '0': exit,
}
"""
